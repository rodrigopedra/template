# Template

<Summary>

![ window](.gitlab/.png)

<Description>

## Build Flatpak

To build a flatpak bundle of Template use the following instructions:

```bash
$ git clone https://invent.kde.org/fhek/template.git
$ cd template
$ flatpak-builder --repo=repo build-dir --force-clean org.kde.template.json --install-deps-from=flathub
$ flatpak build-bundle repo template.flatpak org.kde.template
```

Now you can either double-click the `template.flatpak` file to open it with
some app store (discover, gnome-software, etc...) or run:

```bash
$ flatpak install template.flatpak
```
